## Table of Contents
- [Deployment](#deployment)
  - [Static Server](#static-server)
  - [Surge](#surge)
  
## Deployment
### Static Server

For environments using [Node](https://nodejs.org/), the easiest way to handle this would be to install [serve](https://github.com/zeit/serve) and let it handle the rest:

```sh
npm install -g serve
serve -s build
```

The last command shown above will serve your static site on the port **5000**. Like many of [serve](https://github.com/zeit/serve)’s internal settings, the port can be adjusted using the `-p` or `--port` flags.

Run this command to get a full list of the options available:

```sh
serve -h
```
### [Surge](https://surge.sh/)
Install the Surge CLI if you haven’t already by running `npm install -g surge`. Run the `surge` command and log in you or create a new account.

When asked about the project path, make sure to specify the `build` folder, for example:

```sh
       project: /path/to/project/build
```

```sh
➜  wemoscow git:(master) surge

   Running as bgin@inbox.ru (Student)

            project: /Users/walborn/Projects/wemoscow/build
         domain: wemoscow.surge.sh
         upload: [====================] 100% eta: 0.0s (10 files, 1198134 bytes)
            CDN: [====================] 100%
             IP: 55.55.555.555

   Success! - Published to wemoscow.surge.sh
```

Note that in order to support routers that use HTML5 `pushState` API, you may want to rename the `index.html` in your build folder to `200.html` before deploying to Surge. This [ensures that every URL falls back to that file](https://surge.sh/help/adding-a-200-page-for-client-side-routing).
