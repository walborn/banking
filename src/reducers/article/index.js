import * as constant from '../../actions/article';
import { Map, List } from 'immutable';

export const initialState = Map({
    list: List(),
    item: Map(),
    status: Map({
        list: 'waiting',
        item: 'waiting',
    }),
    count: Map({
        list: null,
    }),
});

export default (state = initialState, { type, payload, meta }) => {
    switch (type) {
        case constant.LIST_REQUEST:
            return state.setIn([ 'status', 'list' ], 'request');
        case constant.LIST_SUCCESS:
            console.log(payload);
            return state
                .set('list', List(payload))
                .setIn([ 'count', 'list' ], payload.count)
                .setIn([ 'status', 'list' ], 'success');
        case constant.LIST_FAILURE:
            return state.setIn([ 'status', 'list' ], 'failure');

        case constant.ITEM_REQUEST:
            return state.setIn([ 'status', 'item' ], 'request');
        case constant.ITEM_SUCCESS:
            return state
                .set('item', Map(payload.result[0]))
                .setIn([ 'status', 'item' ], 'success');
        case constant.ITEM_FAILURE:
            return state.setIn([ 'status', 'item' ], 'failure');

        default:
            return state;
    }
};
