import { combineReducers } from 'redux';
import auth from './auth';
import user from './user';
import article from './article';

export default combineReducers({
    auth,
    user,
    article,
});
