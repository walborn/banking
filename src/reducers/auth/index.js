import { List, Map } from 'immutable';
import * as constant from '../../actions/auth';

export const initialState = Map({
    avatarId: '',
    avatarUrl: '',
    errors: List(),
    loading: false,
    avatarLoading: false,
    me: Map(),
    status: Map({
        signIn: 'waiting',
        signUp: 'waiting',
        signInSocial: 'waiting',
        signUpSocial: 'waiting',
        me: 'waiting',
    }),
    errorMessage: Map({
        signIn: List(),
        signUp: List(),
        signInSocial: List(),
        signUpSocial: List(),
        me: List(),
    }),
});

export default (state = initialState, action) => {
    switch (action.type) {
        case constant.ME_REQUEST:
            return state.setIn([ 'status', 'me' ], 'request');

        case constant.ME_SUCCESS:
            return state
                .setIn([ 'status', 'me' ], 'success')
                .set('me', Map(action.payload.result[0]));

        case constant.ME_FAILURE:
            return state.setIn([ 'status', 'me' ], 'failure');

        case constant.SIGNIN_REQUEST:
            return state.setIn([ 'status', 'signIn' ], 'request');

        case constant.SIGNIN_SUCCESS:
            window.localStorage.setItem(
                'access_token',
                action.payload.result.access_token
            );
            return action.payload.errorCode
                ? state
                    .setIn([ 'status', 'signIn' ], 'failure')
                    .setIn(
                        [ 'errorMessage', 'signIn' ],
                        List([ action.payload.errorMessage ])
                    )
                : state
                    .setIn([ 'status', 'signIn' ], 'success')
                    .setIn([ 'errorMessage', 'signIn' ], List());

        case constant.SIGNIN_FAILURE:
            return state.setIn([ 'status', 'signIn' ], 'failure');

        case constant.SIGNUP_REQUEST:
            return state.setIn([ 'status', 'signUp' ], 'request');

        case constant.SIGNUP_SUCCESS:
            window.localStorage.setItem(
                'access_token',
                action.payload.result.access_token
            );
            return action.payload.errorCode
                ? state
                    .setIn([ 'status', 'signUp' ], 'failure')
                    .setIn([ 'errorMessage', 'signUp' ], List(action.payload.result))
                : state
                    .setIn([ 'status', 'signUp' ], 'success')
                    .setIn([ 'errorMessage', 'signUp' ], List());

        case constant.SIGNUP_FAILURE:
            return state
                .setIn([ 'status', 'signUp' ], 'failure')
                .setIn([ 'error', 'signUp' ], List([ 'Неизвестная ошибка' ]));

        case constant.SIGNIN_SOCIAL_SUCCESS:
            window.localStorage.setItem(
                'access_token',
                action.payload.result.access_token
            );
            return action.payload.errorCode
                ? state
                    .setIn([ 'status', 'signInSocial' ], 'failure')
                    .setIn(
                        [ 'errorMessage', 'signInSocial' ],
                        List([ action.payload.errorMessage ])
                    )
                : state
                    .setIn([ 'status', 'signInSocial' ], 'success')
                    .setIn([ 'errorMessage', 'signInSocial' ], List());

        case constant.SIGNIN_SOCIAL_FAILURE:
            return state.setIn([ 'status', 'signInSocial' ], 'failure');

        case constant.SIGNUP_SOCIAL_REQUEST:
            return state.setIn([ 'status', 'signUpSocial' ], 'request');

        case constant.SIGNUP_SOCIAL_SUCCESS:
            window.localStorage.setItem(
                'access_token',
                action.payload.result.access_token
            );
            return action.payload.errorCode
                ? state
                    .setIn([ 'status', 'signUpSocial' ], 'failure')
                    .setIn(
                        [ 'errorMessage', 'signUpSocial' ],
                        List(action.payload.result)
                    )
                : state
                    .setIn([ 'status', 'signUpSocial' ], 'success')
                    .setIn([ 'errorMessage', 'signUpSocial' ], List());

        case constant.SIGNUP_SOCIAL_FAILURE:
            return state
                .setIn([ 'status', 'signUpSocial' ], 'failure')
                .setIn([ 'error', 'signUpSocial' ], List([ 'Неизвестная ошибка' ]));

        case constant.SIGNOUT:
            window.localStorage.removeItem('access_token');
            return initialState;


        default:
            return state;
    }
};
