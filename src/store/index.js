import { createStore, applyMiddleware } from 'redux';
import { apiMiddleware } from 'redux-api-middleware';
import reducers from '../reducers';
import logger from './middlewares/logger';
import apiMiddlewareBefore from './middlewares/api';

const reduxTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const middlewares = [ apiMiddlewareBefore, apiMiddleware, ...[ process.env.NODE_ENV === 'development' && logger ].filter(Boolean) ]; // remove logger-middleware from production
export default initialState => applyMiddleware(...middlewares)(createStore)(reducers, initialState, reduxTools);