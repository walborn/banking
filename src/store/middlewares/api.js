import { CALL_API } from 'redux-api-middleware';

export default store => next => action => { // eslint-disable-line
    const callApi = action[CALL_API];
    if (callApi) { // Check if this action is a redux-api-middleware action
        callApi.endpoint = `${process.env.REACT_APP_API}${callApi.endpoint}`;
        callApi.headers = { 'Content-Type': 'application/json', ...callApi.headers };
        if ([ 'DELETE', 'PUT', 'POST' ].includes(callApi.method)) callApi.body = JSON.stringify({ access_token: localStorage.getItem('access_token'), ...callApi.body });
    }

    return next(action);
};