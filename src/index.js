import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './store';
import { BrowserRouter } from 'react-router-dom';
import Application from './views/application';
import registerServiceWorker from './registerServiceWorker';
import { me } from 'src/actions/auth';
import './react.magic';


const defaultState = {};
const store = configureStore(defaultState);
const root = document.getElementById('root');

store.dispatch(me());
ReactDOM.render(<BrowserRouter><Provider store={store}><Application/></Provider></BrowserRouter>, root);
registerServiceWorker();
