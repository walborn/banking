import React from "react";
import PropTypes from "prop-types";
import { findDOMNode } from "react-dom";
import ReactDatePicker from "react-datetime";
import CalendarSVG from "src/assets/svg/calendar.js";
import "./styles.css";

/**
 * !!!Warning!!!
 * Здесь используется внешний компонент, который перерисовывается при выборе масштаба времени
 * и теряет родителя, поэтому мы должны ничего не делать, когда поиск родителя у нас натыкается на
 * .rdtTime, .rdtDays, .rdtMonths, .rdtYears
 *
 const closest = (el, fn) => {
    // hack - if we find rdt* class, we do nothing
    const classes = { rdtTime: true, rdtDays: true, rdtMonths: true, rdtYears: true };
    if (el && classes[el.className]) return true;
    return el && (fn(el) ? el : closest(el.parentNode, fn));
};
 */
const lang = {
  time: {
    day: ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
    week: {
      monday: "Пн",
      tuesday: "Вт",
      wednesday: "Ср",
      thursday: "Чт",
      friday: "Пт",
      saturday: "Сб",
      sunday: "Вс"
    },
    month: [
      "Январь",
      "Февраль",
      "Март",
      "Апрель",
      "Май",
      "Июнь",
      "Июль",
      "Август",
      "Сентябрь",
      "Октябрь",
      "Ноябрь",
      "Декабрь"
    ]
  }
};
const toTimeString = (date, format = "DD.MM.YYYY hh:mm", blank = "") => {
  const type = Object.prototype.toString.call(date);
  if (type === "[object Number]") {
    if (date < 10000000000) date *= 1000; // it means we have seconds
    date = new Date(date);
  } else if (type !== "[object Date]") return blank;
  const res = {};
  res.day = date.getDay();
  res.date = `0${date.getDate()}`.slice(-2);
  res.month = `0${date.getMonth() + 1}`.slice(-2);
  res.year = `${date.getFullYear()}`;
  res.hour = `0${date.getHours()}`.slice(-2);
  res.minute = `0${date.getMinutes()}`.slice(-2);
  res.seconds = `0${date.getSeconds()}`.slice(-2);

  return format
    .replace("day", lang.time.day[res.day])
    .replace("month", lang.time.month[+res.month - 1])
    .replace("DD", res.date)
    .replace("MM", res.month)
    .replace("YYYY", res.year)
    .replace("YY", res.year.slice(-2))
    .replace("hh", res.hour)
    .replace("mm", res.minute)
    .replace("ss", res.seconds);
};

export default class DatePicker extends React.Component {
  static defaultProps = {
    showLabel: false,
    defaultValue: new Date()
  };

  static propTypes = {
    onChange: PropTypes.func,
    onClick: PropTypes.func,
    defaultValue: PropTypes.object,
    dateClassName: PropTypes.string,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    showLabel: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      date: props.defaultValue
    };
  }

  componentDidMount() {
    document.addEventListener("click", this.handleDocumentClick);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleDocumentClick);
  }

  minMaxDate = ({ _d }) => {
    const { minDate, maxDate } = this.props;
    const curr = _d.setHours(0, 0, 0);

    if (minDate && maxDate) {
      return curr > minDate && curr < maxDate;
    } else if (minDate) {
      return curr > minDate;
    } else if (maxDate) {
      return curr <= maxDate.setHours(0, 0, 0);
    }

    return true;
  };

  renderDay = (props, currentDate) => {
    const onDateClick = e => {
      this.setState({ open: false });
      if (typeof props.onClick === "function") props.onClick(e);
    };

    return (
      <td {...props} onClick={onDateClick}>
        {currentDate.date()}
      </td>
    );
  };
  renderMonth = (props, month, year) => {
    const onDateClick = e => {
      if (typeof props.onClick === "function") props.onClick(e);
    };
    // fix bag in origin component!
    const date =
      Object.prototype.toString.call(this.state.date) === "[object Date]"
        ? this.state.date
        : new Date();
    const active =
      Object.prototype.toString.call(date) === "[object Date]" &&
      (date.getMonth() === month && date.getFullYear() === year);

    return (
      <td
        {...props}
        className={["rdtMonth", active && "rdtActive"]}
        onClick={onDateClick}
      >
        {this.lang.time.month[month]}
      </td>
    );
  };
  handleChange = moment => {
    this.setState({ date: moment._d });
    if (typeof this.props.onChange === "function")
      this.props.onChange(moment._d);
  };

  handleTriggerClick = e => {
    this.setState({ open: !this.state.open });
    if (typeof this.props.onClick === "function") this.props.onClick(e);
  };

  handleDocumentClick = e => {
    const closest = (el, fn) => {
      // hack - if we find rdt* class, we do nothing
      const classes = {
        rdtTime: true,
        rdtDays: true,
        rdtMonths: true,
        rdtYears: true
      };
      if (el && classes[el.className]) return true;
      return el && (fn(el) ? el : closest(el.parentNode, fn));
    };
    const $parent = findDOMNode(this);
    const open = !!closest(e.target, el => el === $parent);
    return !open && this.setState({ open });
  };

  render() {
    const { dateClassName, showLabel, className } = this.props;
    const { open } = this.state;

    let { defaultValue } = this.props;
    if (Object.prototype.toString.call(defaultValue) !== "[object Date]")
      defaultValue = null;

    return (
      <div
        className={["datepicker", open && "datepicker_open", className]}
        style={{
          position: showLabel ? "absolute" : "relative",
          zIndex: open && 2147483647
        }}
      >
        <div className="datepicker__button" onClick={this.handleTriggerClick}>
          <CalendarSVG className="datepicker__button__icon" />

          {!!showLabel &&
            (defaultValue ? (
              <div className={[dateClassName, "datepicker__button__label"]}>
                {toTimeString(defaultValue, "DD.MM.YYYY")}
              </div>
            ) : (
              <div className={[dateClassName, "datepicker__button__label"]}>
                Не задана
              </div>
            ))}
        </div>
        <div className="datepicker__triangle" />
        <ReactDatePicker
          renderDay={this.renderDay}
          renderMonth={this.renderMonth}
          defaultValue={defaultValue || new Date()}
          onChange={this.handleChange}
          timeFormat={false}
          closeOnSelect
          locale="ru"
          isValidDate={this.minMaxDate}
          input={false}
        />
      </div>
    );
  }
}
