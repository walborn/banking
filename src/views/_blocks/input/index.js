import React, { Component } from "react";
import PropTypes from "prop-types";
import closeSVG from "src/assets/svg/close.svg";
import "./styles.css";

export default class Input extends Component {
  static propTypes = {
    className: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    title: PropTypes.string,
    disabled: PropTypes.bool,
    failure: PropTypes.bool,
    readOnly: PropTypes.bool,
    cleaning: PropTypes.bool,
    onChange: PropTypes.func,
    onDocumentClick: PropTypes.func,
    withReduxForm: PropTypes.bool,
    multiLine: PropTypes.bool
  };
  static defaultProps = {
    type: "text",
    cleaning: true,
    onChange: () => {},
    withReduxForm: false
  };
  state = {
    value: this.props.value
  };

  componentDidMount() {
    document.addEventListener("click", this.handleDocumentClick);
  }

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (value !== this.props.value) this.setState({ value });
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleDocumentClick);
  }

  get value() {
    return this.state.value;
  }

  set value(value) {
    this.setState({ value });
  }

  handleChange = ({ value }) => {
    this.setState({ value }, () =>
      this.props.onChange({ value, name: this.props.name })
    );
  };

  handleClear = event => {
    event.preventDefault();
    const { withReduxForm, onChange } = this.props;

    if (!withReduxForm) {
      this.handleChange({ value: "" });
    } else {
      onChange("");
    }
  };
  handleDocumentClick = event => {
    const closest = (el, fn) =>
      el && (fn(el) ? el : closest(el.parentNode, fn));
    const $parent = this.$input;
    const hitting = closest(event.target, el => el === $parent);
    if (hitting && typeof this.$input.focus === "function") this.$input.focus();
  };

  render() {
    const {
      className,
      title,
      placeholder,
      readOnly,
      onChange,
      disabled,
      cleaning,
      withReduxForm,
      failure,
      onDocumentClick,
      multiLine,
      ...props
    } = this.props;
    const { value } = this.state;
    const cleaner = value && cleaning && !readOnly && !disabled;
    const InputComponent = multiLine ? "textarea" : "input";

    return (
      <div
        ref={r => {
          this.$input = r;
        }}
        className={[
          "input",
          className,
          disabled && "disabled",
          readOnly && "readonly",
          failure && "failure",
          value && title && "with-title",
          cleaner && "with-cleaner"
        ]}
      >
        <InputComponent
          {...props}
          ref={r => {
            this.$input = r;
          }}
          value={value}
          placeholder={placeholder}
          readOnly={readOnly}
          disabled={disabled}
          onChange={e =>
            withReduxForm
              ? onChange(e.target.value)
              : this.handleChange(e.target)
          }
        />
        <div className="input__title">{value && title}</div>
        <img
          src={closeSVG}
          className={[
            "input__cleaner",
            multiLine && "input__cleaner--multiline"
          ]}
          onClick={this.handleClear}
          alt="x"
        />
      </div>
    );
  }
}
