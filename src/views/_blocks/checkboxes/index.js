import React, { Component } from "react";
import PropTypes from "prop-types";
import { CheckBox } from "src/views/_blocks";
import isEqual from "lodash.isequal";
import "./styles.scss";

/**
 * Usage:
 * Необходимо задать названия чекбоксов, а так же их значения с ключами
 * Каждое изменение вызывает событие onChange, в котором передается
 * измененный список { inputKey: isChecked }
 * - labels - список названий чекбоксов
 * - list (isRequired) - список состояний чекбоксов
 * - order - порядок, в котором будет отображаться список
 * - readonly - запрещает изменение выбранных чекбоксов
 *
 <CheckBoxes
 labels={{ seagull: 'Чайка', rabbit: 'Кролик', camel: 'Верблюд' }}
 list={{ seagull: false, rabbit: true, camel: true }}
 order={[ 'camel', 'seagull', 'rabbit' ]}
 onChange={(list, updated, element) => this.setState({ list }, callback) }
 />
 */
export default class CheckBoxes extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    list: PropTypes.object.isRequired,
    order: PropTypes.array,
    className: PropTypes.string,
    readonly: PropTypes.bool,
    onClick: PropTypes.func
  };

  static defaultProps = {
    readonly: false
  };

  state = {
    list: this.props.list
  };

  componentWillMount() {
    this.elements = {};
  }

  componentWillReceiveProps(nextProps) {
    const { list } = nextProps;
    if (!isEqual(list, this.props.list)) this.value = list;
  }

  get value() {
    return Object.keys(this.props.list).reduce(
      (res, i) => ({ ...res, [i]: this.elements[i].value }),
      {}
    );
  }

  set value(list) {
    list =
      list ||
      Object.keys(this.props.list).reduce(
        (res, i) => ({ ...res, [i]: false }),
        {}
      );
    Object.keys(this.elements).forEach(key => {
      this.elements[key].value = list[key];
    });
    this.setState({ list });
  }

  handleChange = key => value => {
    const list = { ...this.state.list, [key]: value };
    this.setState({ list }, () =>
      this.props.onChange(list, isEqual(this.props.list, list), { key, value })
    );
  };

  render() {
    const { className, labels, list, order, readonly } = this.props;
    return (
      <div className={["checkbox-list", className]}>
        {(order || Object.keys(list)).map(key => (
          <CheckBox
            ref={r => {
              this.elements[key] = r;
            }}
            key={key}
            readonly={readonly}
            label={labels[key]}
            checked={list[key]}
            onChange={this.handleChange(key)}
          />
        ))}
      </div>
    );
  }
}
