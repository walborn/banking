# range/date

## Что это?

Компонент выбора интервала по дате

## Пример

```jsx
<Range.Date
    header={{
        from: 'Начало',
        to: 'Окончание',
        className: 'range-date',
    }}
    from={new Date('3/30/2016')}
    to={new Date('10/29/2017')}
    minDate={new Date().setHours(-1, 59, 59)}
    onChange={({ from, to }) => { console.log(from, to); }}
/>
```

## props

* `className` - Задает дополнительный css класс верхнему элементу.
* `from` - Значение [от] типа `Date`.
* `to` - Значение [до] типа `Date`.
* `maxDate` - Верхняя граница по дате, типа `Number` (UTC milliseconds).
* `minDate` - Нижняя граница по дате, типа `Number` (UTC milliseconds).
* `format` - Формат отображения даты, по умолчанию 'DD.MM.YYYY'. Подробнее см `helpers/time.js/toTimeString`.
* `header` - Опции заголовка элемента.
    * `className` для доп стилей заголовка,  
    * `from` для подписи значения [от] и  
    * `to` для подписи значения [до].
* `disabled` - Флаг, запрещает редактирование.
* `placeholders` - Опции placeholder'ов (см ниже).

### `props.placeholders`

* `from` - Опции для верхней границы.  
* `to` - Опции для верхней границы.  

Пример (используется по умолчанию):
```js
placeholders: {
    from: 'Начало',
    to: 'Конец',
}
```