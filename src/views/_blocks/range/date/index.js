import React from "react";
import PropTypes from "prop-types";
import isEqual from "lodash.isequal";
import { DatePicker, Input } from "src/views/_blocks";
import "./styles.css";

const lang = {
  time: {
    day: ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
    week: {
      monday: "Пн",
      tuesday: "Вт",
      wednesday: "Ср",
      thursday: "Чт",
      friday: "Пт",
      saturday: "Сб",
      sunday: "Вс"
    },
    month: [
      "Январь",
      "Февраль",
      "Март",
      "Апрель",
      "Май",
      "Июнь",
      "Июль",
      "Август",
      "Сентябрь",
      "Октябрь",
      "Ноябрь",
      "Декабрь"
    ]
  }
};
const toTimeString = (date, format = "DD.MM.YYYY hh:mm", blank = "") => {
  const type = Object.prototype.toString.call(date);
  if (type === "[object Number]") {
    if (date < 10000000000) date *= 1000; // it means we have seconds
    date = new Date(date);
  } else if (type !== "[object Date]") return blank;
  const res = {};
  res.day = date.getDay();
  res.date = `0${date.getDate()}`.slice(-2);
  res.month = `0${date.getMonth() + 1}`.slice(-2);
  res.year = `${date.getFullYear()}`;
  res.hour = `0${date.getHours()}`.slice(-2);
  res.minute = `0${date.getMinutes()}`.slice(-2);
  res.seconds = `0${date.getSeconds()}`.slice(-2);

  return format
    .replace("day", lang.time.day[res.day])
    .replace("month", lang.time.month[+res.month - 1])
    .replace("DD", res.date)
    .replace("MM", res.month)
    .replace("YYYY", res.year)
    .replace("YY", res.year.slice(-2))
    .replace("hh", res.hour)
    .replace("mm", res.minute)
    .replace("ss", res.seconds);
};

const toDate = date => {
  const type = Object.prototype.toString.call(date);
  if (type === "[object Number]") {
    if (date < 10000000000) date *= 1000; // it means we have seconds
    return new Date(date);
  }
  return type === "[object Date]" ? new Date(date) : null;
};

const roundDate = (date, dt = 0, measure = "ms") => {
  date = toDate(date);
  if (date === null) return null;
  let hours = 0;
  let minutes = 0;
  let seconds = 0;
  let milliseconds = 0;
  switch (measure) {
    case "Y":
      date.setYear(date.getFullYear() + dt);
      break;
    case "M":
      date.setMonth(date.getMonth() + dt);
      break;
    case "D":
      date.setDate(date.getDate() + dt);
      break;
    case "h":
      hours = dt;
      break;
    case "m":
      minutes = dt;
      break;
    case "s":
      seconds = dt;
      break;
    default:
      milliseconds = dt;
      break;
  }
  date.setHours(hours, minutes, seconds, milliseconds);
  return date;
};

export default class RangeDate extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    from: PropTypes.instanceOf(Date),
    to: PropTypes.instanceOf(Date),
    maxDate: PropTypes.instanceOf(Date),
    minDate: PropTypes.instanceOf(Date),
    longLabel: PropTypes.bool,
    format: PropTypes.string,
    disabled: PropTypes.bool
  };
  static defaultProps = {
    format: "DD.MM.YYYY"
  };

  state = {
    labels: this.props.longLabel
      ? { from: "Начало", to: "Окончание" }
      : { from: "С", to: "До" },
    from: this.props.from,
    to: this.props.to
  };

  componentWillReceiveProps(nextProps) {
    const { from, to } = nextProps;
    if (!isEqual(from, this.props.from) || !isEqual(to, this.props.to))
      this.setState({ from, to });
  }

  toTimeString = date => toTimeString(date, this.props.format);

  handleChange = name => date => {
    // для конечной даты мы прибавляем день без миллисекунды
    if (date !== null) date = roundDate(date, 86399000 * (name === "to"));
    this.setState(
      { [name]: date },
      () =>
        typeof this.props.onClick === "function" &&
        this.props.onClick(
          {
            from: this.state.from,
            to: this.state.to
          },
          true
        )
    );
  };

  handleClear = name => () => this.handleChange(name)(null);

  render() {
    const { className, disabled, maxDate, minDate } = this.props;
    const { from, to, labels } = this.state;

    return (
      <div className={["date-range", className]}>
        <div className="date-range__item">
          <Input
            className={[
              "date-range__input",
              disabled && "date-range__input--disabled"
            ]}
            value={this.toTimeString(from)}
            disabled
            cleaning={disabled}
            placeholder={labels.from}
          />
          <DatePicker
            className={[
              "date-range__icon",
              disabled && "date-range__icon--disabled"
            ]}
            defaultValue={this.props.from}
            minDate={roundDate(minDate, -86400000)}
            maxDate={roundDate(to || maxDate, 86399999)}
            onChange={this.handleChange("from")}
            disabled={disabled}
          />
        </div>
        <div className="date-range__item">
          <Input
            className={[
              "date-range__input",
              disabled && "date-range__input--disabled"
            ]}
            value={this.toTimeString(to)}
            disabled
            cleaning={disabled}
            placeholder={labels.to}
          />
          <DatePicker
            className={[
              "date-range__icon",
              disabled && "date-range__icon--disabled"
            ]}
            defaultValue={this.props.to}
            minDate={roundDate(from || minDate, -86400000)}
            maxDate={maxDate}
            onChange={this.handleChange("to")}
            disabled={disabled}
          />
        </div>
      </div>
    );
  }
}
