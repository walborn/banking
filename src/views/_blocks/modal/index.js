import React from "react";
import PropTypes from "prop-types";
import "./styles.css";

const Backdrop = props => (
  <div {...props} className={["modal-base__backdrop", props.className]} />
);
const Container = props => (
  <div {...props} className={["modal-base__container", props.className]} />
);
const Row = props => (
  <div {...props} className={["modal-base__row", props.className]} />
);
const Header = props => (
  <div
    {...props}
    className={["modal-base__row modal-base__header", props.className]}
  />
);
const Body = props => (
  <div
    {...props}
    className={["modal-base__row modal-base__body", props.className]}
  />
);
const Footer = props => (
  <div
    {...props}
    className={["modal_group__row modal-base__footer", props.className]}
  />
);

const shortcutPropTypes = { className: PropTypes.string };

Backdrop.propTypes = shortcutPropTypes;
Container.propTypes = shortcutPropTypes;
Row.propTypes = shortcutPropTypes;
Header.propTypes = shortcutPropTypes;
Body.propTypes = shortcutPropTypes;
Footer.propTypes = shortcutPropTypes;

export default class Modal extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    open: PropTypes.bool,
    onOpen: PropTypes.func,
    onClose: PropTypes.func
  };

  static defaultProps = {
    open: false
  };

  state = {
    open: this.props.open
  };

  componentWillMount() {
    document.querySelector("body").classList.contains("modal-open") && // IE11 bug: https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/11865865/
      document
        .querySelector("body")
        .classList.toggle("modal-open", this.state.open);
  }

  open = () => this.toggle(true, this.props.onOpen);

  close = () => this.toggle(false, this.props.onClose);

  toggle = (open, event) =>
    this.setState({ open }, () => {
      document.querySelector("body").classList.toggle("modal-open", open);
      typeof event === "function" && event();
    });

  render() {
    const {
      className,
      containerClassName,
      width,
      children,
      ...props
    } = this.props;
    const propOpen = props.open;
    delete props.open;
    delete props.onOpen;

    const { open } = this.state;

    return (
      <div className={["modal-base", className, (propOpen || open) && "open"]}>
        <Backdrop onClick={this.close} />
        <Container className={containerClassName} {...props}>
          <span className="modal-base__close" onClick={this.close} />
          {children}
        </Container>
      </div>
    );
  }
}

Modal.Header = Header;
Modal.Body = Body;
Modal.Footer = Footer;
Modal.Row = Row;
Modal.Container = Container;
