import React from "react";
import PropTypes from "prop-types";
import { Spinner, Button } from "src/views/_blocks";
import "./styles.css";

export default class List extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    fetch: PropTypes.func,
    onChange: PropTypes.func,
    onDidMountFetched: PropTypes.func,
    limit: PropTypes.number,
    offset: PropTypes.number,
    count: PropTypes.number,
    list: PropTypes.array
  };

  static defaultProps = {
    limit: 6,
    offset: 0,
    onChange: () => {},
    onDidMountFetched: () => {}
  };
  state = {
    fetching: true,
    offset: this.props.offset,
    limit: this.props.limit
  };
  timeout = null;
  componentWillReceiveProps(nextProps) {
    const { search } = nextProps;
    if (search !== this.props.search) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(
        () => this.handleFetch({ offset: -this.state.limit, search }),
        600
      );
    }
  }

  componentDidMount() {
    const { offset, limit } = this.state;
    this.props.onChange({ limit, offset });
    this.props
      .fetch({ offset, limit })
      .then(response =>
        this.setState({ fetching: false }, () =>
          this.props.onDidMountFetched(response)
        )
      );
  }

  handleFetch = ({
    offset = this.state.offset,
    limit = this.state.limit,
    search = this.props.search
  }) => {
    const params = { offset: offset + limit, limit };
    if (search) params["q[search]"] = search;
    this.setState({ fetching: true, offset: offset + limit }, () =>
      this.props
        .fetch(params)
        .then(() =>
          this.setState({ fetching: false }, () =>
            this.props.onChange({ limit, offset: offset + limit })
          )
        )
    );
  };
  render() {
    const { className, list, count, children } = this.props;
    const { fetching, offset, limit } = this.state;
    return (
      <div className={["list", className]}>
        {children}
        <div
          className="list__more"
          hidden={!fetching && offset + limit >= count}
        >
          <Spinner value={fetching}>
            <Button green disabled={fetching} onClick={this.handleFetch}>
              Загрузить еще...
            </Button>
          </Spinner>
        </div>
      </div>
    );
  }
}
