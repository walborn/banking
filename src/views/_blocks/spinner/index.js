import React from "react";
import PropTypes from "prop-types";
import SpinnerSVG from "src/assets/svg/spinner";
import "./styles.css";

export default class Spinner extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    value: PropTypes.bool
  };
  static defaultProps = {
    value: false
  };

  render() {
    const { className, value, children, ...props } = this.props;

    return (
      <div className={["spinner", className]} {...props}>
        {children}
        {value && <SpinnerSVG className="spinner__icon" />}
      </div>
    );
  }
}
