import React from "react";
import PropTypes from "prop-types";
import "./styles.css";

export default class Button extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    children: PropTypes.node
  };

  render() {
    const { className, children, disabled, ...props } = this.props;
    /* eslint-disable no-mixed-operators */
    const color =
      (props.transparent && "transparent") ||
      (props.green && "green") ||
      (props.blue && "blue") ||
      (props.red && "red") ||
      (props.orange && "orange") ||
      (props.silver && "silver") ||
      (props.green_ && "green_") ||
      (props.green_light && "green_light");
    /* eslint-enable no-mixed-operators */
    delete props[color];
    return (
      <button
        {...props}
        className={["button", className, disabled && "disabled", color]}
        disabled={disabled}
      >
        {children}
      </button>
    );
  }
}
