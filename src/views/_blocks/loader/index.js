import React from "react";
import PropTypes from "prop-types";
import SpinnerSVG from "src/assets/svg/spinner.svg";
import "./styles.css";

export default class Loader extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
  };

  render() {
    const { className, ...props } = this.props;
    const children =
      typeof props.children === "string" ? (
        <p style={{ textAlign: "center", color: "#999" }}>{props.children}</p>
      ) : (
        props.children
      );
    delete props.children;
    return (
      <div {...props} className={[className, "loader"]}>
        <div>
          <img src={SpinnerSVG} alt="" />
          <div>{children}</div>
        </div>
      </div>
    );
  }
}
