import React from "react";
import PropTypes from "prop-types";
import "./styles.css";

/**
 * Usage:
 * Необходимо задать названия чекбоксов, а так же их значения с ключами
 * Каждое изменение вызывает событие onChange, в котором передается
 * измененный список { inputKey: isChecked }
 * - label - название чекбокса, которое видно пользователю
 * - checked - начальное состояние, здесь это либо true либо false, относительно которого смотрится, было ли изменение
 * - readonly - возможность сделать все неактивным
 * - onChange - когда мы кликаем по чекбоксу, то нам возвращается true или false, выбран элемент или нет
 *
 * <CheckBox
 *      label="Кролик"
 *      checked={this.state.suOrigin['rabbit']} // начальное состояние
 *      onChange={value => this.setState({ rabbit: value }, callback)}
 * />
 */
export default class CheckBox extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    checked: PropTypes.bool,
    readonly: PropTypes.bool,
    onChange: PropTypes.func
  };
  static defaultProps = {
    checked: false,
    readonly: false,
    onChange: () => {}
  };
  state = {
    checked: this.props.checked
  };

  componentWillReceiveProps(nextProps) {
    const { checked } = nextProps;
    if (checked !== this.props.checked) this.setState({ checked });
  }

  get value() {
    return this.state.checked;
  }

  set value(checked) {
    this.setState({ checked }, () => this.props.onChange(checked));
  }

  handleChange = ({ checked = !this.state.checked }) => {
    if (this.props.readonly) return;
    this.value = checked;
  };

  render() {
    const { className, label, readonly } = this.props;
    const { checked } = this.state;
    return (
      <fieldset
        className={["checkbox", className, readonly && "radio__readonly"]}
        onClick={this.handleChange}
      >
        <span
          className={[
            "checkbox__square",
            checked && "checked",
            label && "has-label"
          ]}
        >
          <span className="sa-line sa-tip" />
          <span className="sa-line sa-long" />
        </span>
        {label && <span className="checkbox__label">{label}</span>}
      </fieldset>
    );
  }
}
