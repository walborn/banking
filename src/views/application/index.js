import React from 'react';
import Header from './header';
import Main from './main';
import './index.css';


export default () => <Main><Header /></Main>;
