import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Pages from 'src/views/pages';

export default ({ children, ...props }) => (
    <main {...props}>
        {children}
        <Switch>
            <Route exact path="/" component={Pages.Home} />
        </Switch>
    </main>
);
