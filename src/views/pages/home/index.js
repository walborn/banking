import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import * as articleActions from 'src/actions/article';
import Template from './template';

const mapStateToProps = ({ article }) => ({
    articles: article.get('list').toJS(),
});

const mapDispatchToProps = dispatch => ({
    fetchArticles: params => dispatch(articleActions.list(params)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Template));
