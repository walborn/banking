import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';


export default class HomePage extends React.Component {
    static propTypes = {
        fetchArticles: PropTypes.func.isRequired,
        articles: PropTypes.array,
    };

    componentDidMount() {
        window.scrollTo(0, 0);
        this.props.fetchArticles();
    };

    render() {
        const { articles } = this.props;
        return (
            <div id="page" className="home-page" key="page">
                <h1>List of articles</h1>
                {
                    articles.map(i => (<pre>{JSON.stringify(i, null, 3)}</pre>))
                }
            </div>
        );
    }
}
