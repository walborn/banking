import { RSAA } from 'redux-api-middleware';
import { stringify } from './index';

export const LIST_REQUEST = '@@team/LIST_REQUEST';
export const LIST_SUCCESS = '@@team/LIST_SUCCESS';
export const LIST_FAILURE = '@@team/LIST_FAILURE';

export const ITEM_REQUEST = '@@team/ITEM_REQUEST';
export const ITEM_SUCCESS = '@@team/ITEM_SUCCESS';
export const ITEM_FAILURE = '@@team/ITEM_FAILURE';

export const list = params => ({
    [RSAA]: {
        endpoint: `/user/list${stringify(params)}`,
        method: 'GET',
        types: [ LIST_REQUEST, { type: LIST_SUCCESS, meta: params }, LIST_FAILURE ],
    },
});

export const item = id => ({
    [RSAA]: {
        endpoint: `/user/item/${id}`,
        method: 'GET',
        types: [ ITEM_REQUEST, ITEM_SUCCESS, ITEM_FAILURE ],
    },
});