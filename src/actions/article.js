import { RSAA } from 'redux-api-middleware';
import { stringify } from './index';

export const LIST_REQUEST = '@@article/LIST_REQUEST';
export const LIST_SUCCESS = '@@article/LIST_SUCCESS';
export const LIST_FAILURE = '@@article/LIST_FAILURE';

export const ITEM_REQUEST = '@@article/ITEM_REQUEST';
export const ITEM_SUCCESS = '@@article/ITEM_SUCCESS';
export const ITEM_FAILURE = '@@article/ITEM_FAILURE';

export const list = params => ({
    [RSAA]: {
        endpoint: `/articles${stringify(params)}`,
        method: 'GET',
        types: [ LIST_REQUEST, { type: LIST_SUCCESS, meta: params }, LIST_FAILURE ],
    },
});

export const item = id => ({
    [RSAA]: {
        endpoint: `/articles/${id}`,
        method: 'GET',
        types: [ ITEM_REQUEST, ITEM_SUCCESS, ITEM_FAILURE ],
    },
});