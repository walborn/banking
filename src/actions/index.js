export const stringify = params => Object.prototype.toString.call(params) === '[object Object]'
    && `?${Object.entries(params).map(([key, value]) => (value === null ? key : `${key}=${value}`)).join('&')}`
    || '';
